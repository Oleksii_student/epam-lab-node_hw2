const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://alex_boldarev:123alex@cluster0.olhlrcb.mongodb.net/homework?retryWrites=true&w=majority');

const { currentUserRouter} = require('./currentUserRouter')
const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');

const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/users/me', currentUserRouter); 


const start = async () => {
  try {
    app.listen(PORT);
    console.log(`App started at port: ${PORT}`);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}