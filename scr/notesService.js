const { Note } = require('./models/Notes.js');

function createNote(req, res) {
    const { text } = req.body;
    const {userId} = req.user;

    if (!text || !userId || typeof text !== 'string' || typeof userId !== 'string') {
      res.status(400).json({ message: 'Bad request' });
    }
    const note = new Note({
      text,
      userId,
    });
    note.save().then(() => {
      res.status(200).json({
        message: 'Success',
      });
    });
  }

  
  async function getNotes(req, res) {
    const { offset=0, limit=10 } = req.query;
    const { userId } = req.user;
    const notes = await Note.find({userId}).skip(offset).limit(limit);
    const count = await Note.find({userId}).count();

    res.status(200).json({
        offset,
        limit,
        count,
        notes,
      }); 
  }
  

  const getNoteById = (req, res) => {
    if (req.params.id) {
      Note.findById(req.params.id)
        .then((note) => {
          res.status(200).json({
            note: note
          });
        });
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  };
  

  const updateNoteById = async (req, res) => {
    const note = await Note.findById(req.params.id);
    const { text } = req.body;
    if (note && text) {
      note.text = text;
      note.save().then(() => res.status(200).json({ message: 'Success' }));
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  };
  

  const checkCompletedById = async (req, res) => {
    const note = await Note.findById(req.params.id);
    // заменил парам. saved в json на { message: 'Success' } 
    if (note) {
      note.completed = !note.completed;
      note.save().then(() => res.status(200).json({ message: 'Success' })); 
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  };
  

  const deleteNote = (req, res) => {
    if (req.params.id) {
      Note.findByIdAndDelete(req.params.id)
        .then(() => {
          res.status(200).json({ message: 'Success' });
        });
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  };
  

module.exports = {
    createNote,
    getNotes,
    getNoteById,
    updateNoteById,
    checkCompletedById,
    deleteNote
};