const express = require('express');
const router = express.Router();

const { getProfileUser, editPasswordUser, deleteProfileUser } = require('./currentUserService');


const { authMiddleware } = require('./middleware/authMiddleware');


router.get('/', authMiddleware, getProfileUser);    // я оставил в пути me, а у нее нету

router.patch('/', authMiddleware, editPasswordUser);

router.delete('/', authMiddleware, deleteProfileUser);

module.exports = {
    currentUserRouter: router,
};