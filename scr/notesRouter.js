const express = require('express');
const router = express.Router();

const {
    createNote, getNotes, deleteNote, getNoteById, updateNoteById, checkCompletedById
} = require('./notesService.js');

const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes); 

router.get('/:id', authMiddleware, getNoteById); // Вместо getMyBooks сделал getNoteById

router.put('/:id', authMiddleware, updateNoteById);  // Вместо updateMyBookById сделал updateNoteById

router.patch('/:id', authMiddleware, checkCompletedById);  // Вместо markMyBookCompletedById сделал patchCompletedById

router.delete('/:id', authMiddleware, deleteNote);  // Вместо deleteBook сделал  deleteNote

module.exports = {
    notesRouter: router,
};