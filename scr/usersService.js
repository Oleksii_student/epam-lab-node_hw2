const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  console.log('username ==>', username);
  console.log('password ==>', password);

  if (!username || !password || typeof username !== 'string' || typeof password !== 'string') {
    res.status(400).send({ message: 'bad request' });
  }

  const passwordCrypted = await bcrypt.hash(password, 10);
  console.log('passwordCrypted ====>', passwordCrypted);

  const user = new User({
    username,
    password: passwordCrypted
  });
  console.log('user ======>', user);

  user.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch((err) => {
      next(err);
    })
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (!req.body.username || !req.body.password || typeof req.body.password !== 'string' || typeof req.body.username !== 'string') {
    res.status(400).json({ message: 'Bad request' });
  }
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, 'Elton-70-John');
    return res.status(200).json({
      message: 'Success',
      jwt_token: jwtToken,
    });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};

