const mongoose = require('mongoose');

const Note = mongoose.model('note', {
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
  completed: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
});

module.exports = {
  Note,
};

