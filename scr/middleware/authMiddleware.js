const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;
  console.log(authorization);
  
  if (!authorization) {
    return res.status(400).json({ message: 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).json({ message: 'Please, include token to request' });
  }
  try {
    const tokenPayload = jwt.verify(token, 'Elton-70-John');
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
  return 1;
};

module.exports = {
  authMiddleware,
};

