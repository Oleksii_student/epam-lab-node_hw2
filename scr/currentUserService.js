const { User } = require('./models/Users.js');
const bcrypt = require("bcryptjs");

const getProfileUser = (req, res) => {
  if (req.user.userId) {
    User.findById(req.user.userId)
    .then((result) => res.status(200).json({
      user: {
        _id: result._id,
        username: result.username,
        createdDate: result.createdDate,
      },
    }))
  } else {
    res.status(400).json({ message: 'Bad request' });
  }
}


const editPasswordUser = async (req, res) => {
  // TODO encrypt password 2 times
  // const encryptedOldPassword = ....
  // const encryptedNewPassword = .... 
  const encryptedOldPassword = await bcrypt.hash(req.body.oldPassword, 10);
  console.log('encryptedOldPassword ===^=>', encryptedOldPassword);
   
  const encryptedNewPassword = await bcrypt.hash(req.body.newPassword, 10);
  console.log('encryptedNewPassword ===^=>', encryptedNewPassword);
   
  try {
    await User.findOneAndUpdate(
      { _id: req.user.userId, password: encryptedOldPassword },   // было  req.body.oldPassword
      { $set: { password: encryptedNewPassword } }               // было req.body.newPassword
    );
    res.status(200).json({message: "Success"});

  } catch {
    res.status(400).json({ message: 'Something went wrong' });
  }
  // User.findByIdAndUpdate(
  //   { _id: req.user.userId, password: req.body.oldPassword },
  //   // req.user.userId,
  //   { $set: { password: req.body.newPassword } },
  //   (err, updatedDoc) => {
  //     if (err) res.status(400).json({ message: 'Something went wrong' });
  //     return res.status(200).json(updatedDoc);
  //   },
  // );
};


const deleteProfileUser = (req, res) => User.findByIdAndDelete({ _id: req.user.userId })
.then(() => res
  .status(200)
  .json({ message: `User ${req.user.username} was successfully deleted` }))
.catch((err) => res.status(err.status).json({ message: err.message }));



module.exports = {
  getProfileUser,
  editPasswordUser,
  deleteProfileUser
};

